package com.testbot;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;

public class MainActivity extends AppCompatActivity {
    private static final int TIMEOUT = 2000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // overrides some safety checks on opening connections on the main thread
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void runSMTP(View view){

        // make dialog popup
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        try {
            // trying to connect using port 25 should trigger the detector
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress("http://192.168.0.1", 25), TIMEOUT);
            socket.close();
            builder.setMessage("SMTP ran successfully").setTitle("SMTP");
        } catch (Exception e) {
            builder.setMessage("Sent packets on port 25");
            //builder.setMessage("Error running SMTP: " + e.getMessage()).setTitle("SMTP");
        }

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void runIRC(View view){
        // port 6667 associated with IRC; many bot control servers use this port to send commands
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        try {
            // trying to connect using port 6667 should trigger the detector
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress("http://192.168.0.1", 6667), TIMEOUT);
            PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
            out.close();
            socket.close();
        } catch (Exception e) {
            builder.setMessage("Sent packets on port 6667");
            //builder.setMessage("Error running SMTP: " + e.getMessage()).setTitle("SMTP");
        }

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void runDNS(View view){
        // port 53 used for DNS requests
        try{
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress("http://192.168.0.1", 53), TIMEOUT);
            socket.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public void runHttp(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        try{
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress("192.168.0.1", 80), TIMEOUT);
            socket.close();
            builder.setMessage("SMTP ran successfully").setTitle("SMTP");
        }
        catch(Exception e){
            builder.setMessage("Sent packets on port 80");
            e.printStackTrace();
        }

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void runHttp2(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        try{
            URL url = new URL("http://www.google.com");
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            int response = connection.getResponseCode();
            builder.setMessage("Http2 with HttpURLConnection ran with response: " + response).setTitle("Http2");
        }
        catch(Exception e){
            builder.setMessage("Sent packets on port 80");
            e.printStackTrace();
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    // repeatedly makes connection attemps to a list of known c&c servers (ip addresses)
    public void runBlacklistIP(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        String[] ipList = {"101.200.81.187",
                "103.19.89.118",
                "103.230.84.239",
                "103.241.0.100",
                "103.26.128.84",
                "103.4.52.150",
                "103.7.59.135",
                "104.238.158.106",
                "109.127.8.242",
                "109.229.210.250",
                "109.229.36.65",
                "109.248.33.243",
                "112.78.6.234",
                "113.29.230.24",
                "116.193.77.118",
                "185.35.138.22",
                "186.64.120.104",
                "187.174.252.247",
                "188.165.94.96",
                "216.215.112.149",
                "222.29.197.232",
                "31.7.63.146",
                "37.123.99.188"};

        for(int i = 0; i < ipList.length; i++) {
            try{
                URL url = new URL("http://" + ipList[i]);
                URLConnection connection = (HttpURLConnection)url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        builder.setMessage("Ran black list with IP addresses");
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    // repeatedly makes connection attemps to a list of known c&c servers (domain names)
    public void runBlacklistDN(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String[] domainList = {
                "1st.technology",
                "arvision.com.co",
                "atmape.ru",
                "az-armaturen.su",
                "bestdove.in.ua",
                "bright.su",
                "hotelavalon.org",
                "hui-ain-apparel.tk",
                "ice.ip64.net",
                "mxstat230.com",
                "nasscomminc.tk",
                "ns511849.ip-192-99-19.net",
                "ns513726.ip-192-99-148.net",
                "prtscrinsertcn.net",
                "qbhcsope.com",
                "sanyai-love.rmu.ac.th",
                "server.bovine-mena.com",
                "www.witkey.com",
                "zabava-bel.ru",
        };

        for (int i = 0; i < domainList.length; i++) {
            try {
                URL url = new URL("http://" + domainList[i]);
                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        builder.setMessage("Ran black list with IP addresses");
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    // repeatedly make connection requests to known ports used by tracking botnets (honeynet)
    public void runPortList(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        int[] portList = {
                42,
                80,
                81,
                443,
                903,
                1025,
                1433,
                2745,
                3127,
                3306,
                3410,
                5000,
                6129
        };

        for(int i = 0; i < portList.length; i++) {
            try{
                Socket socket = new Socket();
                socket.connect(new InetSocketAddress("192.168.0.1", portList[i]), TIMEOUT);
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        builder.setMessage("Ran known C&C port list");
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}

