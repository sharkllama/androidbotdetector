import subprocess
import datetime
import time
import requests
import json
import codecs

# path to adb installation
adb_path = '/home/swells/Android/Sdk/platform-tools/adb'

# IP geolocation lookup information (ipinfo.io)
ip_lookup_url = 'http://ipinfo.io'

# path to the network log on the android device
network_log_path = '/mnt/sdcard/networklog.txt'

# populate some known botnet ports to check
popular_botnet_ports = ['6667','23', '53', '1080']

# example of some known botnet ips
known_botnet_ips = ["101.200.81.187","103.19.89.118","103.230.84.239","103.241.0.100","103.26.128.84","103.4.52.150","103.7.59.135","104.238.158.106","109.127.8.242","109.229.210.250","109.229.36.65","109.248.33.243","112.78.6.234","113.29.230.24","116.193.77.118","185.35.138.22","186.64.120.104","187.174.252.247","188.165.94.96","216.215.112.149","222.29.197.232","31.7.63.146","37.123.99.188"]

network_log_header = '\"Timestamp\",\"Interface1\",\"Interface2\",\"uID\",\"OriginIP\",\"Origin Port\",\"Origin Port Name\",\"DestinationIP\",\"DestinationPort\",\"Dest Port Name\",\"Length\",\"Protocol\",\"Number of packets\", \"Total Bytes\"'

# Runs the script to build the uid -> app name mapping from the phone
def fetch_uid_apps():
	print('Fetching installed apps on the device...')
	process = subprocess.Popen(adb_path + ' shell < ./resources/adb_scripts/uid_commands.txt > ./temp/uid_app.temp', shell=True)
	process.wait()
	uid_app_map = {}
	# first 4 lines are unneeded
	skip_lines = 4
	with open('./temp/uid_app.temp') as f:
		for line in f:
			if(skip_lines > 0):
				skip_lines -= 1
			else:
				line_split = line.split(' ')
				uid_app_map[line_split[1]] = line_split[0]
	print('Done')
	return uid_app_map

# truncates an ip string to the first 3 parts of the IP  ex('146.234.54.100' -> '146.234.54')
# to minimize the amount of calls to the ip lookup service
def get_remote_subnet(ip):
	res = ip[::-1] # reverse the ip string
	res = res[res.find('.') + 1:]
	res = res[::-1]
	#print (res)
	return res

# Runs the script to extract the network log from the phone
def fetch_network_log():
	print('Fetching most recent network log')
	process = subprocess.Popen(adb_path + ' pull ' + network_log_path + ' && mv networklog.txt ./temp/', shell=True)
	process.wait()
	print('Done.')

# Runs the script to extract the local subnet from the phone
def fetch_local_subnet():
	print('Fetching local IP of the device')
	process = subprocess.Popen(adb_path + ' shell netcfg > ./temp/local_ip.temp', shell=True)
	process.wait()

	local_ip = ''
	with open('./temp/local_ip.temp') as f:
		for line in f:
			try:
				line_split = line.split()
				if 'wlan' in line_split[0]:
					local_ip = line_split[2][:8]
			except Exception:
				continue
	if local_ip == '':
		raise Exception('Could not extract IP from the phone.')
	print('Done. %s' % local_ip)
	return local_ip

# Builds a dictionary containing a mapping from port number to the assigned port name (standard port usage)
def populate_port_identities():
	port_names = {}
	with open('./resources/port_identities.txt') as f:
		for line in f:
			try:
				line_split = line.split(',')
				# port names is a dict using a port number to return the string description
				port_names[line_split[1]] = line_split[0]
			except Exception:
				continue
	return port_names

def print_analysis(app_safety_info):
	print('Dectecting possible malicious applications')
	output_lines = {}

	# read through logs and find the apps that access strange ports
	for app_name in app_safety_info.keys():
		accessed_ports = app_safety_info[app_name]
		for port in accessed_ports.keys():
			if port in popular_botnet_ports:
				if app_name not in output_lines.keys():
					output_lines[app_name] = []
				output_lines[app_name].append(port)

	print('\n\nPossible Malicious applications detected!')
	for app_name in output_lines.keys():
		print('%s : ' % app_name)
		print(output_lines[app_name])


	print('Done')





# MAIN FUNCTION

if __name__ == '__main__':

############################################################
########	Log Analysis set up
############################################################
	# populates uid -> application name mapping
	uid_app_map = fetch_uid_apps()
	
	# retrieves the network log from the phone
	fetch_network_log()
	
	# retrieves the current local ip from the phone (needed to skip some ip lookups)
	local_subnet = fetch_local_subnet()
	
	# builds a dictionary from a file containing the port number to the assigned protocol
	port_names = populate_port_identities()

	
	# Builds the key used to identify a unique communication event
	def gen_key(log_line):
		# 'uid, originIP, originPort, DestIP, DestPort, Protocol'
		return log_line[3] + log_line[4] + log_line[5] + log_line[6] + log_line[7] + log_line[9]

	# Returns the name of the port being used (if it exists)
	def get_port_name(port_num):
		if port_num not in port_names.keys():
			return ''
		return port_names[port_num]

	# returns the name of the app
	def get_app_name(uid):
		# uid labels 0 and -1 designated for root and the kernel
		if uid == '0':
			return 'root'
		if uid == '-1':
			return 'kernel'

		if uid not in uid_app_map.keys():
			return ''
		return uid_app_map[uid]

	# retrieves the location of the determined remote ip using a web service given a raw network log entry
	locations = {}
	def fetch_geolocation(log_line):
		orig_ip = log_line[4]
		dest_ip = log_line[6]
		chosen_ip = local_subnet

		# check which ip is NOT in the local subnet
		if local_subnet in orig_ip and local_subnet not in dest_ip:
			chosen_ip = dest_ip
		elif local_subnet not in orig_ip and local_subnet in dest_ip:
			chosen_ip = orig_ip

		# do not ping server if the ip is not remote
		if local_subnet in chosen_ip:
			return ('','','','','')
		
		# check the cache for this location's identity, fetch if not already seen
		if get_remote_subnet(chosen_ip) not in locations.keys():
			# get location of the remote ip
			r = requests.get(ip_lookup_url + '/' + chosen_ip)
			j = json.loads(r.content)
			# this is pretty ugly, but just makes sure we at least whatever information that we can (if incomplete)
			try:
				location_split = j['loc'].split(',')
				latitude = location_split[0]
				longitude = location_split[1]
			except Exception:
				latitude, longitude = ('n/a','n/a')
			try:
				print('IP Lookup: %s, %s, %s, %s, coords: %s, %s' % (chosen_ip, j['city'], j['region'], j['country'], latitude, longitude))
				locations[get_remote_subnet(chosen_ip)] = (j['city'], j['region'], j['country'], latitude, longitude)
			except KeyError:
				return ('','','','','')
		return locations[get_remote_subnet(chosen_ip)]

#################################################################################################################
########## Build the resulting log file
#################################################################################################################
	
	app_safety_info = {}
	def analyze_log_item(log_line):
		app_name = get_app_name(log_line[3])
		origin_port = log_line[5]
		dest_port = log_line[7]
		if app_name not in app_safety_info.keys():
			app_safety_info[app_name] = {}
		app_safety_info[app_name][origin_port] = 1
		app_safety_info[app_name][dest_port] = 1

	output_lines = {}
	output_line_keys = ['uID','AppName','OriginIP','OriginPort','OriginPortName','DestinationIP','DestinationPort','DestPortName','Protocol','TotalPackets','TotalBytes','RemoteCity', 'RemoteRegion', 'RemoteCountry', 'Latitude', 'Longitude']
	output_line_format_string = '%s,\"%s\",\"%s\",%s,\"%s\",\"%s\",%s,\"%s\",\"%s\",%s,%s,\"%s\",\"%s\",\"%s\",%s,%s\n'

	def gen_new_log_item(log_line):
		city, region, country, latitude, longitude = fetch_geolocation(log_line)
		analyze_log_item(log_line)
		return {'uID' 				: log_line[3],
				'AppName'			: get_app_name(log_line[3]),
				'OriginIP'			: log_line[4],
				'OriginPort'		: log_line[5],
				'OriginPortName'	: get_port_name(log_line[5]),
				'DestinationIP'		: log_line[6],
				'DestinationPort'	: log_line[7],
				'DestPortName'		: get_port_name(log_line[7]),
				'Protocol'			: log_line[9][:-1], # chop of trailing \n
				'TotalPackets'		: 1, # start off this new log item with one packet counted so far
				'TotalBytes'		: int(log_line[8]), # convert the length to number of bytes (to accumulate total)
				'RemoteCity'		: city,
				'RemoteRegion'		: region,
				'RemoteCountry'		: country,
				'Latitude'			: latitude,
				'Longitude'			: longitude
				}

	def increment_log_item(log_line):
		key = gen_key(log_line)
		# add 1 to the total packets sent by this connection
		output_lines[key]['TotalPackets'] += 1
		# add number of bytes to the total
		output_lines[key]['TotalBytes'] += int(log_line[8])

	
	############################################################################################################
	# This reads in the raw log and generates a new log with the total packets and bytes sent on this connection
	############################################################################################################
	with open('./temp/networklog.txt') as f:
		for line in f:
			line_split = line.split(',')
			curr_key = gen_key(line_split)
			if curr_key not in output_lines.keys():
				output_lines[curr_key] = gen_new_log_item(line_split)
			else:
				increment_log_item(line_split)

	########################
	# write the new log file
	########################

	# constructs an output line in our new network log
	def get_output_strings():
		for key in output_lines.keys():
			yield output_line_format_string % tuple([output_lines[key][item] for item in output_line_keys])

	# get timestamp for the new log
	timestamp_str = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H:%M:%S')

	with codecs.open('./logs/networklog_' + timestamp_str + '.txt', mode='w+', encoding='utf-8') as w:
		# write header
		w.write(output_line_format_string % tuple([key for key in output_line_keys]))
		for output_string in get_output_strings():
			try:
				w.write(output_string)
			except Exception as e:
				continue

	#print ('Log created : ' % 'networklog_' + timestamp_str + '.txt')
	print ('Done')

	print_analysis(app_safety_info)