
if __name__ == '__main__':
	with open('./port_identities_fixed.csv', 'w') as w:
		w.write('\"Service Name\",\"Port Number\",\"Protocol\",\"Description\"\n')
		with open('./port_identites.csv') as f:
			for line in f:
				try:
					line_split = line.split(',')
					port_num = line_split[1]
					if line_split[3][:-1] == 'Unassigned':
						raise Exception('meh')
					if '-' in port_num:
						port_range = port_num.split('-')
						total_range = int(port_range[1]) - int(port_range[0])
						for x in xrange(total_range + 1):
							item = '\"%s\",%s,\"%s\",\"%s\"\n' % (line_split[0], str(int(port_range[0]) + x), line_split[2], line_split[3][:-1])
							w.write(item)
					else:
						w.write('\"%s\",%s,\"%s\",\"%s\"\n' % (line_split[0], line_split[1], line_split[2], line_split[3][:-1]))
				except Exception as e:
					print e
					continue

